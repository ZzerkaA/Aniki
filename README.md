# Aniki : Yet another android MyAnimeList client

## F-Droid

<img src="https://img.shields.io/f-droid/v/xyz.adjutor.aniki?color=%23D52C70&style=for-the-badge">

F-Droid is an installable catalogue of FOSS apps for android.

<a href="https://f-droid.org/en/packages/xyz.adjutor.aniki/"><img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" height="75"></a>


Initial merge request : [https://gitlab.com/fdroid/fdroiddata/-/merge_requests/9027](https://gitlab.com/fdroid/fdroiddata/-/merge_requests/9027)

## Features

+ List of elements used in top manga and top anime with a recycler view.

SwipeRefresh used to refresh the list of data of the first page.

Use the button (prev and next) to navigate with ease through the pages.

<br/>

+ Details of a chosen element from the recycler view with an intent object.

Title, synopsys and background selectable.

URL link openable.

<br/>

+ Search pages for animes and mangas.

They display the data in a recycler view similar to the tops.

A feature has been added that hides the keyboard when the query is submitted by the user.

We can also submit the query by clicking the "search" button that replace the "return" one.

Clicking on a searched item also opens a detailed page.

I'm not adding the "multiple pages" feature to the search section because I assume the user should use a more precise query if what he's looking for doesn't show up in the first page.

## API
Multiple calls of the REST API from [jikan.moe](https://jikan.moe).

Usage of :

+ /v3/top/manga
+ /v3/top/anime
+ /v3/manga
+ /v3/anime
+ /v3/search/manga
+ /v3/search/anime

## Data Storage
Data storage with sharedpreferences used for the recycler view of top manga and top anime as well as the details.

Saves the detail page of an item when opened, not when the list is shown.

Usage of ScrollView in the detail page. Thus, items with long synopsis and background aren't cut down.

## Design
Used my "personal" theme colors for the app.

It consists of some colors of the pink/purple "family" :

+ very dark purple : #09022A
+ very light magenta : #FF70FF
+ strong pink : #D52C70
+ slightly desaturated magenta : #C583B6
+ grayish magenta : #C9A6C9

Also added the "bangers" font for the titles.

## Gitflow
Development done with a Gitflow workflow. I used the master, release, develop and feature branches.
Later, I started to add tags.

## Architecture
I used the MVC (Model, View, Controller) architecture.

## Singletons
Usage of singletons.

Used to call APIs and store the api url.

## Usage
### Swipe refresh

<img src="https://adjutor.xyz/gif/refresh-feature.gif" alt="swipe_refresh" width="350"/>

### Previous
Displaying a snack when hitting the previously button on the first page :

<img src="https://adjutor.xyz/gif/prev-button-message.gif" alt="previous_button_on_first_page" width="350"/>

This is a reminder since there's a bug (or a feature ;) ) that resets the page number variable but not the actual list displayed.

### Next

<img src="https://adjutor.xyz/gif/next-button.gif" alt="next_button" width="350"/>

### Text selection and openable link

<img src="https://adjutor.xyz/gif/text-selection-and-link.gif" alt="next_button" width="350"/>

### Search

<img src="https://adjutor.xyz/gif/search-feature.gif" alt="search_pages" width="350"/>

Searching for an item using 2 or less characters will result in an API error. This error is normal, it comes from the REST API I'm using.

<img src="https://adjutor.xyz/gif/search-error.gif" alt="search_error" width="350"/>
