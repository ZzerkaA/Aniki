package xyz.adjutor.aniki.presentation.view.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import xyz.adjutor.aniki.R
import xyz.adjutor.aniki.presentation.controller.SearchAnimeController
import xyz.adjutor.aniki.presentation.model.SearchAnime
import xyz.adjutor.aniki.presentation.view.activity.MainActivity
import xyz.adjutor.aniki.presentation.view.adapter.SearchAnimeAdapter


class SearchAnimeFragment : Fragment() {

    lateinit var controller: SearchAnimeController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.search_anime_page, container, false)

        controller = SearchAnimeController()
        controller.onStart(this)

        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //button to return to the home page
        view.findViewById<Button>(R.id.button_home).setOnClickListener {
            findNavController().navigate(R.id.action_SearchAnimePage_to_HomePage)
        }

        view.findViewById<Button>(R.id.button_query).setOnClickListener {
            val userInput = view.findViewById<TextInputEditText>(R.id.tiet_query).text.toString()
            hideKeyboard()
            controller.updateList(userInput)
        }

        view.findViewById<TextInputEditText>(R.id.tiet_query)
            .setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    val userInput =
                        view.findViewById<TextInputEditText>(R.id.tiet_query).text.toString()
                    hideKeyboard()
                    controller.updateList(userInput)
                    return@OnEditorActionListener true
                }
                false
            })

    }

    private fun hideKeyboard() {
        val activity = activity as MainActivity

        val view = activity.currentFocus
        if (view != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    //display the recyclerview
    fun showList(view: View, animeList: List<SearchAnime>) {
        val recyclerView: RecyclerView = view.findViewById(R.id.recycler_view)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(view.context)
        recyclerView.adapter = SearchAnimeAdapter(animeList)
        (recyclerView.adapter as SearchAnimeAdapter).notifyDataSetChanged()
    }


    //display a snack
    fun showError() {
        Snackbar.make(
            requireView(),
            "API ERROR : Verify your internet connection or your query.",
            Snackbar.LENGTH_LONG
        )
            .setAction("Action", null).show()
    }

}