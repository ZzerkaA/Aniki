package xyz.adjutor.aniki.presentation.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import xyz.adjutor.aniki.R

class HomePage : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.home_page, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<Button>(R.id.button_top_manga).setOnClickListener {
            findNavController().navigate(R.id.action_HomePage_to_TopMangaPage)
        }
        view.findViewById<Button>(R.id.button_top_anime).setOnClickListener {
            findNavController().navigate(R.id.action_HomePage_to_TopAnimePage)
        }
        view.findViewById<Button>(R.id.button_search_manga).setOnClickListener {
            findNavController().navigate(R.id.action_HomePage_to_SearchMangaPage)
        }
        view.findViewById<Button>(R.id.button_search_anime).setOnClickListener {
            findNavController().navigate(R.id.action_HomePage_to_SearchAnimePage)
        }
    }
}