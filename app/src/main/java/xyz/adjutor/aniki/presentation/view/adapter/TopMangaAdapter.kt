package xyz.adjutor.aniki.presentation.view.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import xyz.adjutor.aniki.R
import xyz.adjutor.aniki.presentation.model.TopManga
import xyz.adjutor.aniki.presentation.view.activity.DetailTopMangaActivity

class TopMangaAdapter(private val mangaList: List<TopManga>) :
    RecyclerView.Adapter<TopMangaAdapter.MangaViewHolder>() {

    // Describes an item view and its place within the RecyclerView
    class MangaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val mangaTitle: TextView = itemView.findViewById(R.id.tv_title)
        val mangaRank: TextView = itemView.findViewById(R.id.tv_rank)
        val mangaScore: TextView = itemView.findViewById(R.id.tv_score)
        val mangaImage: ImageView = itemView.findViewById(R.id.iv_image)
        val cardview: CardView = itemView.findViewById(R.id.cv_cardView)
    }

    // Returns a new ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MangaViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_layout, parent, false)

        return MangaViewHolder(view)
    }

    // Returns size of data list
    override fun getItemCount(): Int {
        return mangaList.size
    }

    // Displays data at a certain position
    override fun onBindViewHolder(holder: MangaViewHolder, position: Int) {
        val currentManga: TopManga = mangaList[position]
        holder.mangaTitle.text = currentManga.title
        holder.mangaRank.text = currentManga.rank.toString()
        holder.mangaScore.text = currentManga.score.toString()
        val image: String = currentManga.image_url.toString()
        Glide
            .with(holder.itemView.context)
            .load(image)
            .apply(RequestOptions().override(400))
            .into(holder.mangaImage)

        //when you click on a selected cardview, some datas are sent to the other activity
        holder.cardview.setOnClickListener {
            val currentMangaId = "themangaid"
            val currentMangaTitle = "themangatitle"
            val currentMangaRank = "themangarank"
            val currentMangaScore = "themangascore"
            val currentMangaImageUrl = "themangaimageurl"
            val currentMangaVolumes = "themangavolumes"
            val currentMangaStartDate = "themangastartdate"
            val currentMangaEndDate = "themangaenddate"
            val currentMangaUrl = "themangaurl"

            //intent is used to pass data to another activity
            val intent: Intent =
                Intent(holder.itemView.context, DetailTopMangaActivity::class.java).apply {
                    putExtra(currentMangaId, currentManga.mal_id.toString())
                    putExtra(currentMangaTitle, currentManga.title)
                    putExtra(currentMangaRank, currentManga.rank.toString())
                    putExtra(currentMangaScore, currentManga.score.toString())
                    putExtra(currentMangaImageUrl, currentManga.image_url.toString())
                    putExtra(currentMangaVolumes, currentManga.volumes.toString())
                    putExtra(currentMangaStartDate, currentManga.start_date)
                    putExtra(currentMangaEndDate, currentManga.end_date.toString())
                    putExtra(currentMangaUrl, currentManga.url.toString())
                }
            holder.itemView.context.startActivity(intent)
        }
    }
}