package xyz.adjutor.aniki.presentation

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import xyz.adjutor.aniki.data.*
import xyz.adjutor.aniki.presentation.Constants.Companion.baseUrl

class Singletons {

    companion object {

        val gson: Gson = GsonBuilder()
            .setLenient()
            .create()

        val topMangaApi: TopMangaApi = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(TopMangaApi::class.java)

        val searchMangaApi: SearchMangaApi = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(SearchMangaApi::class.java)

        val mangaApi: MangaApi = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(MangaApi::class.java)

        val topAnimeApi: TopAnimeApi = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(TopAnimeApi::class.java)

        val searchAnimeApi: SearchAnimeApi = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(SearchAnimeApi::class.java)

        val animeApi: AnimeApi = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(AnimeApi::class.java)
    }

}
