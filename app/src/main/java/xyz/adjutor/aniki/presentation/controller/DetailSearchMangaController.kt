package xyz.adjutor.aniki.presentation.controller

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import xyz.adjutor.aniki.presentation.Singletons
import xyz.adjutor.aniki.presentation.model.response.MangaResponse
import xyz.adjutor.aniki.presentation.view.activity.DetailSearchMangaActivity

class DetailSearchMangaController {

    lateinit var view: DetailSearchMangaActivity

    fun onStart(DetailSearchMangaActivity: DetailSearchMangaActivity, mangaId: String) {

        view = DetailSearchMangaActivity

        makeApiCall(mangaId)
    }

    private fun makeApiCall(mangaId: String) {

        Singletons
            .mangaApi
            .getMangaData(mangaId) //based on the id
            .enqueue(object : Callback<MangaResponse> {
                override fun onResponse(
                    call: Call<MangaResponse>,
                    response: Response<MangaResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) { //if the code returned is >= 200 and < 300 AND the the body ain't empty

                        val manga = response.body() //getting the MangaResponse fields
                        view.showDetail(manga!!)

                    } else {
                    view.showError("API ERROR : is not successful")
                }
            }

            override fun onFailure(call: Call<MangaResponse>, t: Throwable) {
                view.showError("API ERROR : onFailure")
            }

        })
    }

}