package xyz.adjutor.aniki.presentation.controller

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.reflect.TypeToken
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import xyz.adjutor.aniki.presentation.Singletons
import xyz.adjutor.aniki.presentation.Singletons.Companion.gson
import xyz.adjutor.aniki.presentation.model.response.AnimeResponse
import xyz.adjutor.aniki.presentation.view.activity.DetailTopAnimeActivity
import java.lang.reflect.Type

class DetailTopAnimeController {

    private lateinit var sharedPreferences: SharedPreferences
    lateinit var view: DetailTopAnimeActivity

    fun onStart(DetailTopAnimeActivity: DetailTopAnimeActivity, animeId: String) {

        view = DetailTopAnimeActivity
        sharedPreferences =
            view.applicationContext.getSharedPreferences("sp_anime", Context.MODE_PRIVATE)

        val anime: AnimeResponse? = getDataFromCache(animeId)
        if (anime != null) {
            view.showDetail(anime)
        } else {
            //taking the API's fields I want and displaying them
            makeApiCall(animeId)
        }
    }

    private fun getDataFromCache(animeId: String): AnimeResponse? {
        val jsonAnime: String? = sharedPreferences.getString(animeId, null)

        return if (jsonAnime == null) {
            null
        } else {
            val type: Type = object : TypeToken<AnimeResponse>() {}.type
            gson.fromJson(jsonAnime, type)
        }
    }

    private fun makeApiCall(animeId: String) {

        Singletons
            .animeApi
            .getAnimeData(animeId) //based on the id
            .enqueue(object : Callback<AnimeResponse> {
                override fun onResponse(
                    call: Call<AnimeResponse>,
                    response: Response<AnimeResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) { //if the code returned is >= 200 and < 300 AND the the body ain't empty

                        val anime = response.body() //getting the AnimeResponse fields
                        saveList(anime)
                        view.showDetail(anime!!)

                } else {
                    view.showError("API ERROR : is not successful")
                }
            }

            override fun onFailure(call: Call<AnimeResponse>, t: Throwable) {
                view.showError("API ERROR : onFailure")
            }

        })
    }

    fun saveList(anime: AnimeResponse?) {
        val jsonString: String = gson.toJson(anime)

        sharedPreferences
            .edit()
            .putString(anime?.mal_id.toString(), jsonString)
            .apply()
    }

}