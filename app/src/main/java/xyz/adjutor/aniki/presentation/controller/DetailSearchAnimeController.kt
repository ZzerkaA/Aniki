package xyz.adjutor.aniki.presentation.controller

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import xyz.adjutor.aniki.presentation.Singletons
import xyz.adjutor.aniki.presentation.model.response.AnimeResponse
import xyz.adjutor.aniki.presentation.view.activity.DetailSearchAnimeActivity

class DetailSearchAnimeController {

    lateinit var view: DetailSearchAnimeActivity

    fun onStart(DetailSearchAnimeActivity: DetailSearchAnimeActivity, animeId: String) {

        view = DetailSearchAnimeActivity

        makeApiCall(animeId)
    }

    private fun makeApiCall(animeId: String) {

        Singletons
            .animeApi
            .getAnimeData(animeId) //based on the id
            .enqueue(object : Callback<AnimeResponse> {
                override fun onResponse(
                    call: Call<AnimeResponse>,
                    response: Response<AnimeResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) { //if the code returned is >= 200 and < 300 AND the the body ain't empty

                        val anime = response.body() //getting the AnimeResponse fields
                        view.showDetail(anime!!)

                    } else {
                    view.showError("API ERROR : is not successful")
                }
            }

            override fun onFailure(call: Call<AnimeResponse>, t: Throwable) {
                view.showError("API ERROR : onFailure")
            }

        })
    }

}