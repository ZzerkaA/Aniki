package xyz.adjutor.aniki.presentation.controller

import android.content.Context
import android.content.SharedPreferences
import android.view.View
import com.google.gson.reflect.TypeToken
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import xyz.adjutor.aniki.presentation.Singletons
import xyz.adjutor.aniki.presentation.Singletons.Companion.gson
import xyz.adjutor.aniki.presentation.model.TopAnime
import xyz.adjutor.aniki.presentation.model.response.TopAnimeResponse
import xyz.adjutor.aniki.presentation.view.fragment.TopAnimeFragment
import java.lang.reflect.Type
import kotlin.properties.Delegates

class TopAnimeController {

    private lateinit var sharedPreferences: SharedPreferences
    private var page by Delegates.notNull<Int>()
    lateinit var view: TopAnimeFragment

    fun onStart(topAnimeFragment: TopAnimeFragment, viewTopAnimePage: View) {

        view = topAnimeFragment
        page = 1
        sharedPreferences =
            viewTopAnimePage.context.getSharedPreferences("sp_anime", Context.MODE_PRIVATE)


        val animeList: List<TopAnime>? = getDataFromCache()
        if (animeList != null) {
            view.showList(viewTopAnimePage, animeList)
        } else {
            makeApiCall(view, 1)
        }
    }

    private fun makeApiCall(view: TopAnimeFragment, page: Int) {

        Singletons
            .topAnimeApi
            .getTopAnimeData(page)
            .enqueue(object : Callback<TopAnimeResponse> {
                override fun onResponse(
                    call: Call<TopAnimeResponse>,
                    response: Response<TopAnimeResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) { //if the code returned is >= 200 and < 300 AND the the body ain't empty

                        val animeList: List<TopAnime> = response.body()!!
                            .getResults() //getting the "top" field containing our list of TopAnimes
                        saveList(animeList)
                        view.showList(
                        view.requireView(),
                        animeList
                    ) //calling the method in charge of displaying on the recyclerview

                } else {
                    view.showError() //a snackbar
                }
            }

            override fun onFailure(call: Call<TopAnimeResponse>, t: Throwable) {
                view.showError()
            }

        })
    }

    private fun saveList(animeList: List<TopAnime>) {
        val jsonString: String = gson.toJson(animeList)

        sharedPreferences
            .edit()
            .putString("jsonAnimeList", jsonString)
            .apply()
    }

    private fun getDataFromCache(): List<TopAnime>? {
        //the value of the animeList json, if nothing is found, return null
        val jsonAnime: String? = sharedPreferences.getString("jsonAnimeList", null)

        //if it's null, well, return null
        return if (jsonAnime == null) {
            null
        } else { //else deserialize the list and return it
            val listType: Type = object : TypeToken<List<TopAnime>>() {}.type
            gson.fromJson(jsonAnime, listType)
        }
    }


    fun onButtonPrevClick() {
        if (page > 1) { // if we're not on the first page, because we can't go back if we're on the first one.
            page -= 1
            makeApiCall(view, page)
            view.showText("Page $page has been loaded.")
        } else {
            view.showText("You're already on page 1. (If you're actually not, refresh the list.)")
        }
    }

    fun onButtonNextClick() {
        page += 1
        makeApiCall(view, page)
        view.showText("Page $page has been loaded.")
    }

    fun updateList() {
        makeApiCall(view, 1)
        view.showText("Data refreshed")
        page = 1
    }

}