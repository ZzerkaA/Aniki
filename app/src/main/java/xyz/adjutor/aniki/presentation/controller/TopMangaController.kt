package xyz.adjutor.aniki.presentation.controller

import android.content.Context
import android.content.SharedPreferences
import android.view.View
import com.google.gson.reflect.TypeToken
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import xyz.adjutor.aniki.presentation.Singletons
import xyz.adjutor.aniki.presentation.Singletons.Companion.gson
import xyz.adjutor.aniki.presentation.model.TopManga
import xyz.adjutor.aniki.presentation.model.response.TopMangaResponse
import xyz.adjutor.aniki.presentation.view.fragment.TopMangaFragment
import java.lang.reflect.Type
import kotlin.properties.Delegates

class TopMangaController {

    private lateinit var sharedPreferences: SharedPreferences
    private var page by Delegates.notNull<Int>()
    lateinit var view: TopMangaFragment

    fun onStart(topMangaPage: TopMangaFragment, viewTopMangaPage: View) {

        view = topMangaPage
        page = 1
        sharedPreferences =
            viewTopMangaPage.context.getSharedPreferences("sp_manga", Context.MODE_PRIVATE)


        val mangaList: List<TopManga>? = getDataFromCache()
        if (mangaList != null) {
            view.showList(viewTopMangaPage, mangaList)
        } else {
            makeApiCall(view, 1)
        }
    }

    private fun makeApiCall(view: TopMangaFragment, page: Int) {

        Singletons
            .topMangaApi
            .getTopMangaData(page)
            .enqueue(object : Callback<TopMangaResponse> {
                override fun onResponse(
                    call: Call<TopMangaResponse>,
                    response: Response<TopMangaResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) { //if the code returned is >= 200 and < 300 AND the the body ain't empty

                        val mangaList: List<TopManga> = response.body()!!
                            .getResults() //getting the "top" field containing our list of TopMangas
                        saveList(mangaList)
                        view.showList(
                        view.requireView(),
                        mangaList
                    ) //calling the method in charge of displaying on the recyclerview

                } else {
                    view.showError() //a snackbar
                }
            }

            override fun onFailure(call: Call<TopMangaResponse>, t: Throwable) {
                view.showError()
            }

        })
    }

    private fun saveList(mangaList: List<TopManga>) {
        val jsonString: String = gson.toJson(mangaList)

        sharedPreferences
            .edit()
            .putString("jsonMangaList", jsonString)
            .apply()
    }

    private fun getDataFromCache(): List<TopManga>? {
        //the value of the mangaList json, if nothing is found, return null
        val jsonManga: String? = sharedPreferences.getString("jsonMangaList", null)

        //if it's null, well, return null
        return if (jsonManga == null) {
            null
        } else { //else deserialize the list and return it
            val listType: Type = object : TypeToken<List<TopManga>>() {}.type
            gson.fromJson(jsonManga, listType)
        }
    }


    fun onButtonPrevClick() {
        if (page > 1) { // if we're not on the first page, because we can't go back if we're on the first one.
            page -= 1
            makeApiCall(view, page)
            view.showText("Page $page has been loaded.")
        } else {
            view.showText("You're already on page 1. (If you're actually not, refresh the list.)")
        }
    }

    fun onButtonNextClick() {
        page += 1
        makeApiCall(view, page)
        view.showText("Page $page has been loaded.")
    }

    fun updateList() {
        makeApiCall(view, 1)
        view.showText("Data refreshed")
        page = 1
    }

}