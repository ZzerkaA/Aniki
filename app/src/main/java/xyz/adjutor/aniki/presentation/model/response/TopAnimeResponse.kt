package xyz.adjutor.aniki.presentation.model.response

import com.google.gson.annotations.SerializedName
import xyz.adjutor.aniki.presentation.model.TopAnime

class TopAnimeResponse {

    @SerializedName("top")
    var top: List<TopAnime>? = null

    fun getResults(): List<TopAnime> {
        return top!!
    }
}