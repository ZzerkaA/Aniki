package xyz.adjutor.aniki.presentation.model.response

import com.google.gson.annotations.SerializedName

class MangaResponse { //only kept the infos I didn't have and that were interesting to me.

    @SerializedName("mal_id")
    var mal_id: Int? = null

    @SerializedName("chapters")
    var chapters: Int? = null

    @SerializedName("synopsis")
    var synopsis: String? = null

    @SerializedName("rank")
    var rank: Int? = null //added for the search feature (detail)

    @SerializedName("background")
    var background: String? = null //a bit of background story about the manga

}
