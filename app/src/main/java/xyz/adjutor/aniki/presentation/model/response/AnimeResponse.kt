package xyz.adjutor.aniki.presentation.model.response

import com.google.gson.annotations.SerializedName

class AnimeResponse { //only kept the infos I didn't have and that were interesting to me.

    @SerializedName("mal_id")
    var mal_id: Int? = null

    @SerializedName("rank")
    var rank: Int? = null //added for the search feature (detail)

    @SerializedName("synopsis")
    var synopsis: String? = null

}
