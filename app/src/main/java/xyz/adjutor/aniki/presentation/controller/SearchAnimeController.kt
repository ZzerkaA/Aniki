package xyz.adjutor.aniki.presentation.controller

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import xyz.adjutor.aniki.presentation.Singletons
import xyz.adjutor.aniki.presentation.model.SearchAnime
import xyz.adjutor.aniki.presentation.model.response.SearchAnimeResponse
import xyz.adjutor.aniki.presentation.view.fragment.SearchAnimeFragment

class SearchAnimeController {

    lateinit var view: SearchAnimeFragment

    fun onStart(searchAnimeFragment: SearchAnimeFragment) {

        view = searchAnimeFragment
    }

    //call the API and show the list
    private fun makeApiCall(view: SearchAnimeFragment, query: String) {

        Singletons
            .searchAnimeApi
            .getSearchAnimeData(q = query)
            .enqueue(object : Callback<SearchAnimeResponse> {
                override fun onResponse(
                    call: Call<SearchAnimeResponse>,
                    response: Response<SearchAnimeResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) { //if the code returned is >= 200 and < 300 AND the the body ain't empty

                        val animeList: List<SearchAnime> = response.body()!!
                            .getResults() //getting the "search" field containing our list of SearchAnimes

                        view.showList(
                        view.requireView(),
                        animeList
                    ) //calling the method in charge of displaying on the recyclerview

                } else {
                    view.showError() //a snackbar
                }
            }

            override fun onFailure(call: Call<SearchAnimeResponse>, t: Throwable) {
                view.showError()
            }

        })
    }

    fun updateList(userInput: String) {
        makeApiCall(view, userInput)
    }

}