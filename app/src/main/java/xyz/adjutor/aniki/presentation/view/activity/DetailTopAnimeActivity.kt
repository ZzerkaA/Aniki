package xyz.adjutor.aniki.presentation.view.activity

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import xyz.adjutor.aniki.R
import xyz.adjutor.aniki.presentation.controller.DetailTopAnimeController
import xyz.adjutor.aniki.presentation.model.response.AnimeResponse

class DetailTopAnimeActivity : AppCompatActivity() {

    lateinit var controller: DetailTopAnimeController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_top_anime)

        controller = DetailTopAnimeController()

        val intentAnimeId = "theanimeid"
        val intentAnimeTitle = "theanimetitle"
        val intentAnimeRank = "theanimerank"
        val intentAnimeScore = "theanimescore"
        val intentAnimeImageUrl = "theanimeimageurl"

        val intentAnimeEpisodes = "theanimeepisodes"
        val intentAnimeStartDate = "theanimestartdate"
        val intentAnimeEndDate = "theanimeenddate"
        val intentAnimeUrl = "theanimeurl"

        val animeId = intent.getStringExtra(intentAnimeId)
        val animeTitle = intent.getStringExtra(intentAnimeTitle)
        val animeRank = intent.getStringExtra(intentAnimeRank)
        val animeScore = intent.getStringExtra(intentAnimeScore)
        val animeImageUrl = intent.getStringExtra(intentAnimeImageUrl)

        val animeEpisodes = intent.getStringExtra(intentAnimeEpisodes)
        val animeStartDate = intent.getStringExtra(intentAnimeStartDate)
        val animeEndDate = intent.getStringExtra(intentAnimeEndDate)
        val animeUrl = intent.getStringExtra(intentAnimeUrl)

        val tvId: TextView = findViewById(R.id.tv_detail_id)
        val tvTitle: TextView = findViewById(R.id.tv_detail_title)
        val tvRank: TextView = findViewById(R.id.tv_detail_rank)
        val tvScore: TextView = findViewById(R.id.tv_detail_score)
        val ivImage: ImageView = findViewById(R.id.iv_detail_image)

        val tvEpisodes: TextView = findViewById(R.id.tv_episodes)
        val tvStartDate: TextView = findViewById(R.id.tv_start_date)
        val tvEndDate: TextView = findViewById(R.id.tv_end_date)
        val tvUrl: TextView = findViewById(R.id.tv_url)

        tvId.text = animeId
        tvTitle.text = animeTitle
        tvRank.text = animeRank
        tvScore.text = animeScore
        Glide
            .with(this)
            .load(animeImageUrl)
            .apply(RequestOptions().override(400))
            .into(ivImage)

        //using null as a string because it has been converted to a string before
        tvEpisodes.text = if (animeEpisodes != "null") {
            animeEpisodes
        } else {
            fieldIsNull()
        }

        tvStartDate.text = animeStartDate

        tvEndDate.text = if (animeEndDate != "null") {
            animeEndDate
        } else {
            fieldIsNull()
        }

        tvUrl.text = animeUrl

        controller.onStart(this, animeId.toString())

    }

    fun showDetail(anime: AnimeResponse) {
        //elements from AnimeResponse
        val tvSynopsis: TextView = findViewById(R.id.tv_synopsis)

        tvSynopsis.text = anime.synopsis.toString()

    }

    fun showError(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }

    private fun fieldIsNull(): String {
        return "Unknown"
    }

}