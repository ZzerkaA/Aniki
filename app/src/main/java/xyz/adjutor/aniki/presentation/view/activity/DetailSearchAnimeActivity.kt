package xyz.adjutor.aniki.presentation.view.activity

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import xyz.adjutor.aniki.R
import xyz.adjutor.aniki.presentation.controller.DetailSearchAnimeController
import xyz.adjutor.aniki.presentation.model.response.AnimeResponse

class DetailSearchAnimeActivity : AppCompatActivity() {

    lateinit var controller: DetailSearchAnimeController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_search_anime)

        controller = DetailSearchAnimeController()

        //used in the list
        val intentAnimeImageUrl = "theanimeimageurl"
        val intentAnimeTitle = "theanimetitle"
        val intentAnimeScore = "theanimescore"

        //only used for the detail
        val intentAnimeId = "theanimeid"
        val intentAnimeUrl = "theanimeurl"
        val intentAnimeEpisodes = "theanimeepisodes"
        val intentAnimeStartDate = "theanimestartdate"
        val intentAnimeEndDate = "theanimeenddate"

        val animeImageUrl = intent.getStringExtra(intentAnimeImageUrl)
        val animeTitle = intent.getStringExtra(intentAnimeTitle)
        val animeScore = intent.getStringExtra(intentAnimeScore)

        val animeId = intent.getStringExtra(intentAnimeId)
        val animeUrl = intent.getStringExtra(intentAnimeUrl)
        val animeEpisodes = intent.getStringExtra(intentAnimeEpisodes)
        val animeStartDate = intent.getStringExtra(intentAnimeStartDate)
        val animeEndDate = intent.getStringExtra(intentAnimeEndDate)


        val ivImage: ImageView = findViewById(R.id.iv_detail_image)
        val tvTitle: TextView = findViewById(R.id.tv_detail_title)
        val tvScore: TextView = findViewById(R.id.tv_detail_score)

        val tvId: TextView = findViewById(R.id.tv_detail_id)
        val tvUrl: TextView = findViewById(R.id.tv_url)
        val tvEpisodes: TextView = findViewById(R.id.tv_episodes)
        val tvStartDate: TextView = findViewById(R.id.tv_start_date)
        val tvEndDate: TextView = findViewById(R.id.tv_end_date)

        Glide
            .with(this)
            .load(animeImageUrl)
            .apply(RequestOptions().override(400))
            .into(ivImage)
        tvTitle.text = animeTitle
        tvScore.text = animeScore


        tvId.text = animeId
        tvUrl.text = animeUrl

        //using null as a string because it has been converted to a string before
        tvEpisodes.text = if (animeEpisodes != "null") {
            animeEpisodes
        } else {
            fieldIsNull()
        }

        tvStartDate.text = splitDate(animeStartDate!!)

        tvEndDate.text = if (animeEndDate != "null") {
            splitDate(animeEndDate!!)
        } else {
            fieldIsNull()
        }

        controller.onStart(this, animeId.toString())

    }

    private fun splitDate(animeDate: String): CharSequence {
        val delimiter = "T"
        return animeDate
            .split(delimiter) //split between the date and the time
            .toTypedArray()[0] //convert it to an array and take the first string

    }

    fun showDetail(anime: AnimeResponse) {
        //elements from AnimeResponse
        val tvSynopsis: TextView = findViewById(R.id.tv_synopsis)
        val tvRank: TextView = findViewById(R.id.tv_detail_rank)

        tvSynopsis.text = anime.synopsis.toString()

        tvRank.text = anime.rank.toString()

    }

    fun showError(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }

    private fun fieldIsNull(): String {
        return "Unknown"
    }
}