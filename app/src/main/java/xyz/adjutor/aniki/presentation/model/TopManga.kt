package xyz.adjutor.aniki.presentation.model

import com.google.gson.annotations.SerializedName

//model
//Content of the top field from the api of top manga
class TopManga {

    @SerializedName("mal_id")
    var mal_id: Int? = null

    @SerializedName("rank")
    var rank: Int? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("url")
    var url: String? = null

    @SerializedName("volumes")
    var volumes: Int? = null

    @SerializedName("start_date")
    var start_date: String? = null

    @SerializedName("end_date")
    var end_date: String? = null

    @SerializedName("score")
    var score: Float? = null

    @SerializedName("image_url")
    var image_url: String? = null

}
