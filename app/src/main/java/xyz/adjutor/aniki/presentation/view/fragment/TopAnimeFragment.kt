package xyz.adjutor.aniki.presentation.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import xyz.adjutor.aniki.R
import xyz.adjutor.aniki.presentation.controller.TopAnimeController
import xyz.adjutor.aniki.presentation.model.TopAnime
import xyz.adjutor.aniki.presentation.view.adapter.TopAnimeAdapter

//view
class TopAnimeFragment : Fragment() {

    lateinit var controller: TopAnimeController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.top_anime_page, container, false)

        controller = TopAnimeController()
        controller.onStart(this, view)

        return view

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //button to return to the home page
        view.findViewById<Button>(R.id.button_home).setOnClickListener {
            findNavController().navigate(R.id.action_TopAnimePage_to_HomePage)
        }
        view.findViewById<Button>(R.id.button_prev).setOnClickListener {
            controller.onButtonPrevClick()
        }
        view.findViewById<Button>(R.id.button_next).setOnClickListener {
            controller.onButtonNextClick()
        }

        //refresh when swiping down at the top of the page
        val swipeRefresh: SwipeRefreshLayout = view.findViewById(R.id.swiperefresh)
        swipeRefresh.setOnRefreshListener {
            controller.updateList()
            swipeRefresh.isRefreshing = false
        }

    }

    //display the recyclerview
    fun showList(view: View, animeList: List<TopAnime>) {
        val recyclerView: RecyclerView = view.findViewById(R.id.recycler_view)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(view.context)
        recyclerView.adapter = TopAnimeAdapter(animeList)
        (recyclerView.adapter as TopAnimeAdapter).notifyDataSetChanged()
    }

    fun showError() {
        Snackbar.make(
            requireView(),
            "API ERROR : Verify your internet connection.",
            Snackbar.LENGTH_LONG
        )
            .setAction("Action", null).show()
    }

    fun showText(text: String) {
        Snackbar.make(
            requireView(),
            text,
            Snackbar.LENGTH_SHORT
        )
            .setAction("Action", null).show()
    }

}