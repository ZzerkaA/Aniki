package xyz.adjutor.aniki.presentation.view.activity

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import xyz.adjutor.aniki.R
import xyz.adjutor.aniki.presentation.controller.DetailSearchMangaController
import xyz.adjutor.aniki.presentation.model.response.MangaResponse

class DetailSearchMangaActivity : AppCompatActivity() {

    lateinit var controller: DetailSearchMangaController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_search_manga)

        controller = DetailSearchMangaController()

        //used in the list
        val intentMangaImageUrl = "themangaimageurl"
        val intentMangaTitle = "themangatitle"
        val intentMangaScore = "themangascore"

        //only used for the detail
        val intentMangaId = "themangaid"
        val intentMangaUrl = "themangaurl"
        val intentMangaChapters = "themangachapters"
        val intentMangaVolumes = "themangavolumes"
        val intentMangaStartDate = "themangastartdate"
        val intentMangaEndDate = "themangaenddate"

        val mangaImageUrl = intent.getStringExtra(intentMangaImageUrl)
        val mangaTitle = intent.getStringExtra(intentMangaTitle)
        val mangaScore = intent.getStringExtra(intentMangaScore)

        val mangaId = intent.getStringExtra(intentMangaId)
        val mangaUrl = intent.getStringExtra(intentMangaUrl)
        val mangaChapters = intent.getStringExtra(intentMangaChapters)
        val mangaVolumes = intent.getStringExtra(intentMangaVolumes)
        val mangaStartDate = intent.getStringExtra(intentMangaStartDate)
        val mangaEndDate = intent.getStringExtra(intentMangaEndDate)


        val ivImage: ImageView = findViewById(R.id.iv_detail_image)
        val tvTitle: TextView = findViewById(R.id.tv_detail_title)
        val tvScore: TextView = findViewById(R.id.tv_detail_score)

        val tvId: TextView = findViewById(R.id.tv_detail_id)
        val tvUrl: TextView = findViewById(R.id.tv_url)
        val tvChapters: TextView = findViewById(R.id.tv_chapters)
        val tvVolumes: TextView = findViewById(R.id.tv_volumes)
        val tvStartDate: TextView = findViewById(R.id.tv_start_date)
        val tvEndDate: TextView = findViewById(R.id.tv_end_date)

        Glide
            .with(this)
            .load(mangaImageUrl)
            .apply(RequestOptions().override(400))
            .into(ivImage)
        tvTitle.text = mangaTitle
        tvScore.text = mangaScore


        tvId.text = mangaId
        tvUrl.text = mangaUrl

        //using null as a string because it has been converted to a string before
        tvChapters.text = if (mangaChapters != "null") {
            mangaChapters
        } else {
            fieldIsNull()
        }

        tvVolumes.text = if (mangaVolumes != "null") {
            mangaVolumes
        } else {
            fieldIsNull()
        }

        tvStartDate.text = splitDate(mangaStartDate!!)

        tvEndDate.text = if (mangaEndDate != "null") {
            splitDate(mangaEndDate!!)
        } else {
            fieldIsNull()
        }

        controller.onStart(this, mangaId.toString())

    }

    private fun splitDate(mangaDate: String): CharSequence {
        val delimiter = "T"
        return mangaDate
            .split(delimiter) //split between the date and the time
            .toTypedArray()[0] //convert it to an array and take the first string

    }

    fun showDetail(manga: MangaResponse) {
        //elements from MangaResponse
        val tvSynopsis: TextView = findViewById(R.id.tv_synopsis)
        val tvRank: TextView = findViewById(R.id.tv_detail_rank)
        val tvBackground: TextView = findViewById(R.id.tv_background)

        tvSynopsis.text = manga.synopsis.toString()

        tvRank.text = manga.rank.toString()

        tvBackground.text = if (manga.background != null) {
            manga.background.toString()
        } else {
            fieldIsNull()
        }

    }

    fun showError(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }

    private fun fieldIsNull(): String {
        return "Unknown"
    }
}