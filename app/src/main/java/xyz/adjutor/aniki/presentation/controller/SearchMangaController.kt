package xyz.adjutor.aniki.presentation.controller

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import xyz.adjutor.aniki.presentation.Singletons
import xyz.adjutor.aniki.presentation.model.SearchManga
import xyz.adjutor.aniki.presentation.model.response.SearchMangaResponse
import xyz.adjutor.aniki.presentation.view.fragment.SearchMangaFragment

class SearchMangaController {

    lateinit var view: SearchMangaFragment

    fun onStart(searchMangaFragment: SearchMangaFragment) {

        view = searchMangaFragment
    }

    //call the API and show the list
    private fun makeApiCall(view: SearchMangaFragment, query: String) {

        Singletons
            .searchMangaApi
            .getSearchMangaData(q = query)
            .enqueue(object : Callback<SearchMangaResponse> {
                override fun onResponse(
                    call: Call<SearchMangaResponse>,
                    response: Response<SearchMangaResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) { //if the code returned is >= 200 and < 300 AND the the body ain't empty

                        val mangaList: List<SearchManga> = response.body()!!
                            .getResults() //getting the "search" field containing our list of SearchMangas

                        view.showList(
                        view.requireView(),
                        mangaList
                    ) //calling the method in charge of displaying on the recyclerview

                } else {
                    view.showError() //a snackbar
                }
            }

            override fun onFailure(call: Call<SearchMangaResponse>, t: Throwable) {
                view.showError()
            }

        })
    }

    fun updateList(userInput: String) {
        makeApiCall(view, userInput)
    }

}