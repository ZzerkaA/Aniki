package xyz.adjutor.aniki.presentation.view.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import xyz.adjutor.aniki.R

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
    }
}