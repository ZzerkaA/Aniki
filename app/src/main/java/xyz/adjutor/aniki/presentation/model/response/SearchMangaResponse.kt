package xyz.adjutor.aniki.presentation.model.response

import com.google.gson.annotations.SerializedName
import xyz.adjutor.aniki.presentation.model.SearchManga

class SearchMangaResponse { //only kept the infos I didn't have and that were interesting to me.

    @SerializedName("results")
    private lateinit var results: List<SearchManga>

    fun getResults(): List<SearchManga> {
        return results
    }
}
