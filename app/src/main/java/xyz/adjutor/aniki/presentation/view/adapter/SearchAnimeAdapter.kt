package xyz.adjutor.aniki.presentation.view.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import xyz.adjutor.aniki.R
import xyz.adjutor.aniki.presentation.model.SearchAnime
import xyz.adjutor.aniki.presentation.view.activity.DetailSearchAnimeActivity

class SearchAnimeAdapter(private val animeList: List<SearchAnime>) :
    RecyclerView.Adapter<SearchAnimeAdapter.AnimeViewHolder>() {

    // Describes an item view and its place within the RecyclerView
    class AnimeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val animeTitle: TextView = itemView.findViewById(R.id.tv_title)
        val animeRank: TextView = itemView.findViewById(R.id.tv_rank)
        val animeScore: TextView = itemView.findViewById(R.id.tv_score)
        val animeImage: ImageView = itemView.findViewById(R.id.iv_image)
        val cardview: CardView = itemView.findViewById(R.id.cv_cardView)
    }

    // Returns a new ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnimeViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_layout, parent, false)

        return AnimeViewHolder(view)
    }

    // Returns size of data list
    override fun getItemCount(): Int {
        return animeList.size
    }

    // Displays data at a certain position
    override fun onBindViewHolder(holder: AnimeViewHolder, position: Int) {
        val currentAnime: SearchAnime = animeList[position]
        holder.animeTitle.text = currentAnime.title
        holder.animeRank.text = "" //the rank isn't supplied by this API
        holder.animeScore.text = currentAnime.score.toString()
        val image: String = currentAnime.image_url.toString()
        Glide
            .with(holder.itemView.context)
            .load(image)
            .apply(RequestOptions().override(400))
            .into(holder.animeImage)

        //when you click on a selected cardview, some datas are sent to the other activity
        holder.cardview.setOnClickListener {
            val currentAnimeId = "theanimeid"
            val currentAnimeUrl = "theanimeurl"
            val currentAnimeImageUrl = "theanimeimageurl"
            val currentAnimeTitle = "theanimetitle"
            val currentAnimeEpisodes = "theanimeepisodes"
            val currentAnimeScore = "theanimescore"
            val currentAnimeStartDate = "theanimestartdate"
            val currentAnimeEndDate = "theanimeenddate"

            //intent is used to pass data to another activity

            val intent: Intent =
                Intent(holder.itemView.context, DetailSearchAnimeActivity::class.java).apply {
                    putExtra(currentAnimeId, currentAnime.mal_id.toString())
                    putExtra(currentAnimeUrl, currentAnime.url.toString())
                    putExtra(currentAnimeImageUrl, currentAnime.image_url.toString())
                    putExtra(currentAnimeTitle, currentAnime.title)
                    putExtra(currentAnimeEpisodes, currentAnime.episodes.toString())
                    putExtra(currentAnimeScore, currentAnime.score.toString())
                    putExtra(currentAnimeStartDate, currentAnime.start_date)
                    putExtra(currentAnimeEndDate, currentAnime.end_date.toString())
                }
            holder.itemView.context.startActivity(intent)
        }

    }
}