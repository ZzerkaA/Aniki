package xyz.adjutor.aniki.presentation.model.response

import com.google.gson.annotations.SerializedName
import xyz.adjutor.aniki.presentation.model.TopManga

class TopMangaResponse {

    @SerializedName("top")
    private lateinit var top: List<TopManga>

    fun getResults(): List<TopManga> {
        return top
    }
}