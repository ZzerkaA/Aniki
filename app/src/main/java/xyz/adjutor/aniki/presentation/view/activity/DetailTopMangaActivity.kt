package xyz.adjutor.aniki.presentation.view.activity

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import xyz.adjutor.aniki.R
import xyz.adjutor.aniki.presentation.controller.DetailTopMangaController
import xyz.adjutor.aniki.presentation.model.response.MangaResponse

class DetailTopMangaActivity : AppCompatActivity() {

    lateinit var controller: DetailTopMangaController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_top_manga)

        controller = DetailTopMangaController()

        //used in the list
        val intentMangaTitle = "themangatitle"
        val intentMangaRank = "themangarank"
        val intentMangaScore = "themangascore"
        val intentMangaImageUrl = "themangaimageurl"

        //only used for the detail
        val intentMangaId = "themangaid"
        val intentMangaVolumes = "themangavolumes"
        val intentMangaStartDate = "themangastartdate"
        val intentMangaEndDate = "themangaenddate"
        val intentMangaUrl = "themangaurl"

        val mangaTitle = intent.getStringExtra(intentMangaTitle)
        val mangaRank = intent.getStringExtra(intentMangaRank)
        val mangaScore = intent.getStringExtra(intentMangaScore)
        val mangaImageUrl = intent.getStringExtra(intentMangaImageUrl)

        val mangaId = intent.getStringExtra(intentMangaId)
        val mangaVolumes = intent.getStringExtra(intentMangaVolumes)
        val mangaStartDate = intent.getStringExtra(intentMangaStartDate)
        val mangaEndDate = intent.getStringExtra(intentMangaEndDate)
        val mangaUrl = intent.getStringExtra(intentMangaUrl)

        val tvTitle: TextView = findViewById(R.id.tv_detail_title)
        val tvRank: TextView = findViewById(R.id.tv_detail_rank)
        val tvScore: TextView = findViewById(R.id.tv_detail_score)
        val ivImage: ImageView = findViewById(R.id.iv_detail_image)

        val tvId: TextView = findViewById(R.id.tv_detail_id)
        val tvVolumes: TextView = findViewById(R.id.tv_volumes)
        val tvStartDate: TextView = findViewById(R.id.tv_start_date)
        val tvEndDate: TextView = findViewById(R.id.tv_end_date)
        val tvUrl: TextView = findViewById(R.id.tv_url)

        tvTitle.text = mangaTitle
        tvRank.text = mangaRank
        tvScore.text = mangaScore
        Glide
            .with(this)
            .load(mangaImageUrl)
            .apply(RequestOptions().override(400))
            .into(ivImage)

        tvId.text = mangaId

        //using null as a string because it has been converted to a string before
        tvVolumes.text = if (mangaVolumes != "null") {
            mangaVolumes
        } else {
            fieldIsNull()
        }

        tvStartDate.text = mangaStartDate

        tvEndDate.text = if (mangaEndDate != "null") {
            mangaEndDate
        } else {
            fieldIsNull()
        }

        tvUrl.text = mangaUrl

        controller.onStart(this, mangaId.toString())

    }

    fun showDetail(manga: MangaResponse) {
        //elements from MangaResponse
        val tvChapters: TextView = findViewById(R.id.tv_chapters)
        val tvSynopsis: TextView = findViewById(R.id.tv_synopsis)
        val tvBackground: TextView = findViewById(R.id.tv_background)

        tvChapters.text = if (manga.chapters != null) {
            manga.chapters.toString()
        } else {
            fieldIsNull()
        }

        tvSynopsis.text = manga.synopsis.toString()

        tvBackground.text = if (manga.background != null) {
            manga.background.toString()
        } else {
            fieldIsNull()
        }

    }

    fun showError(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }

    private fun fieldIsNull(): String {
        return "Unknown"
    }

}