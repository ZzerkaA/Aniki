package xyz.adjutor.aniki.presentation.controller

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.reflect.TypeToken
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import xyz.adjutor.aniki.presentation.Singletons
import xyz.adjutor.aniki.presentation.Singletons.Companion.gson
import xyz.adjutor.aniki.presentation.model.response.MangaResponse
import xyz.adjutor.aniki.presentation.view.activity.DetailTopMangaActivity
import java.lang.reflect.Type

class DetailTopMangaController {

    private lateinit var sharedPreferences: SharedPreferences
    lateinit var view: DetailTopMangaActivity

    fun onStart(DetailTopMangaActivity: DetailTopMangaActivity, mangaId: String) {

        view = DetailTopMangaActivity
        sharedPreferences =
            view.applicationContext.getSharedPreferences("sp_manga", Context.MODE_PRIVATE)

        val manga: MangaResponse? = getDataFromCache(mangaId)
        if (manga != null) {
            view.showDetail(manga)
        } else {
            //taking the API's fields I want and displaying them
            makeApiCall(mangaId)
        }
    }

    private fun getDataFromCache(mangaId: String): MangaResponse? {
        val jsonManga: String? = sharedPreferences.getString(mangaId, null)

        return if (jsonManga == null) {
            null
        } else {
            val type: Type = object : TypeToken<MangaResponse>() {}.type
            gson.fromJson(jsonManga, type)
        }
    }

    private fun makeApiCall(mangaId: String) {

        Singletons
            .mangaApi
            .getMangaData(mangaId) //based on the id
            .enqueue(object : Callback<MangaResponse> {
                override fun onResponse(
                    call: Call<MangaResponse>,
                    response: Response<MangaResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) { //if the code returned is >= 200 and < 300 AND the the body ain't empty

                        val manga = response.body() //getting the MangaResponse fields
                        saveList(manga)
                        view.showDetail(manga!!)

                } else {
                    view.showError("API ERROR : is not successful")
                }
            }

            override fun onFailure(call: Call<MangaResponse>, t: Throwable) {
                view.showError("API ERROR : onFailure")
            }

        })
    }

    fun saveList(manga: MangaResponse?) {
        val jsonString: String = gson.toJson(manga)

        sharedPreferences
            .edit()
            .putString(manga?.mal_id.toString(), jsonString)
            .apply()
    }

}