package xyz.adjutor.aniki.presentation.model

import com.google.gson.annotations.SerializedName

class SearchManga {

    @SerializedName("mal_id")
    var mal_id: Int? = null

    @SerializedName("url")
    var url: String? = null

    @SerializedName("image_url")
    var image_url: String? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("chapters")
    var chapters: Int? = null

    @SerializedName("volumes")
    var volumes: Int? = null

    @SerializedName("score")
    var score: Float? = null

    @SerializedName("start_date") //we'll maybe remove this later
    var start_date: String? = null

    @SerializedName("end_date") //we'll maybe remove this later
    var end_date: String? = null

}