package xyz.adjutor.aniki.data

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import xyz.adjutor.aniki.presentation.model.response.AnimeResponse

interface AnimeApi {

    @GET("v3/anime/{id}")
    fun getAnimeData(@Path("id") id: String): Call<AnimeResponse>

}