package xyz.adjutor.aniki.data

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import xyz.adjutor.aniki.presentation.model.response.TopMangaResponse

interface TopMangaApi {

    @GET("v3/top/manga/{page}")
    fun getTopMangaData(@Path("page") page: Int): Call<TopMangaResponse>

}