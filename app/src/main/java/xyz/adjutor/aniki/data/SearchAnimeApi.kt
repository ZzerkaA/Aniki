package xyz.adjutor.aniki.data

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import xyz.adjutor.aniki.presentation.model.response.SearchAnimeResponse

interface SearchAnimeApi {

    @GET("v3/search/anime")
    fun getSearchAnimeData(@Query("q") q: String): Call<SearchAnimeResponse>

}