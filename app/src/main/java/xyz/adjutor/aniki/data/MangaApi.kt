package xyz.adjutor.aniki.data

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import xyz.adjutor.aniki.presentation.model.response.MangaResponse

interface MangaApi {

    @GET("v3/manga/{id}")
    fun getMangaData(@Path("id") id: String): Call<MangaResponse>

}