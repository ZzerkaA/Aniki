package xyz.adjutor.aniki.data

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import xyz.adjutor.aniki.presentation.model.response.SearchMangaResponse

interface SearchMangaApi {

    @GET("v3/search/manga")
    fun getSearchMangaData(@Query("q") q: String): Call<SearchMangaResponse>

}